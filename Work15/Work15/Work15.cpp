﻿#include <iostream>

void FindOddNumbers(int Limit, bool IsOdd)
{
	for (int i = IsOdd; i <= Limit; i += 2)
	{
		std::cout << i << " ";
	}
}

int main()
{
	FindOddNumbers(10, false);

	return 0;
}